package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {
    public boolean allEqual(String str) {
        if(str.equals("")){
            return false;
        }
        String lowerStr = str.toLowerCase();
        for (int i = 0; i < lowerStr.length(); i++){
            if (lowerStr.charAt(i) != lowerStr.charAt(0)){
                return false;
            }
        }
        return true;
    }

    public Map<String, Long> letterCount(String str) {
        Map<String, Long> result = new HashMap<>();
        String lowerStr = str.toLowerCase();
        for (int i = 0; i < lowerStr.length(); i++){
            result.merge(String.valueOf((lowerStr.charAt(i))), 1L, (a, b) -> a+b);
        }
        return result;
    }

    public String interleave(List<String> listOne, List<String> listTwo) {
        String result = "";
        for(int i = 0; i < listOne.size(); i++){
            result += listOne.get(i);
            result += listTwo.get(i);
        }
        return result;
    }
}
