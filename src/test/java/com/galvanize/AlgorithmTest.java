package com.galvanize;

import org.junit.jupiter.api.Test;


import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {

    @Test
    public void testAllEqualSameLetterDifferentCases(){
        Algorithm algo = new Algorithm();
        boolean trueCase = algo.allEqual("aAa");
        assertTrue(trueCase);

    }
    @Test
    public void testAllEqualDifferentLetters(){
        Algorithm algo = new Algorithm();
        boolean falseCaseOne = algo.allEqual("bbBbabbb");
        assertFalse(falseCaseOne);

    }
    @Test
    public void testAllEqualEmptyString(){
        Algorithm algo = new Algorithm();
        boolean falseCaseTwo = algo.allEqual("");
        assertFalse(falseCaseTwo);
    }
    @Test
    public void testLetterCountOneLetter(){
        Algorithm algo = new Algorithm();
        Map<String, Long> actual = algo.letterCount("aa");
        Map<String, Long> expected = new HashMap<>();
        expected.put("a",(long) 2);
        assertEquals(expected, actual);
    }
    @Test
    public void testLetterCountSeveralLetters(){
        Algorithm algo = new Algorithm();
        Map<String, Long> actual = algo.letterCount("abBcd");
        Map<String, Long> expected = new HashMap<>();
        expected.put("a",1L);
        expected.put("b", 2L);
        expected.put("c", 1L);
        expected.put("d", 1L);
        assertEquals(expected, actual);
    }
    @Test
    public void testLetterCountEmptyString(){
        Algorithm algo = new Algorithm();
        Map<String, Long> actual = algo.letterCount("");
        Map<String, Long> expected = new HashMap<>();
        assertEquals(expected, actual);
    }

    @Test
    public void testInterleaveOne(){
        Algorithm algo = new Algorithm();
        String actual = algo.interleave(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));
        String expected = "adbecf";
        assertEquals(expected, actual);
    }
    @Test
    public void testInterleaveTwo(){
        Algorithm algo = new Algorithm();
        String actual = algo.interleave(Arrays.asList("a", "c", "e"), Arrays.asList("b", "d", "f"));
        String expected = "abcdef";
        assertEquals(expected, actual);
    }
    @Test
    public void testInterleaveEmpty(){
        Algorithm algo = new Algorithm();
        String actual = algo.interleave(Collections.emptyList(), Collections.emptyList());
        String expected = "";
        assertEquals(expected, actual);
    }
}
